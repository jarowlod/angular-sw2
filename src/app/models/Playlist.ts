export interface Playlist {
  id: number | string;
  name: string;
  favorite: boolean;
  /**
   * Hex Color, ie. #ff00ff
   */
  color: string;
  tracks?: Track[]; // or Array<Track>
  // [plackiKey:string]: string
}

export interface Track {
  id: number;
  name: string;
}

// type someStringMap = {
//   [key: string]: string;
// };
