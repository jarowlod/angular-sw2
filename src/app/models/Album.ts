// https://developer.spotify.com/documentation/web-api/reference/

export interface Entity {
  id: string;
  name: string;
}

export interface Album extends Entity {
  images: AlbumImage[];
  artists?: Artist[];
}
export interface Artist extends Entity {}

export interface AlbumImage {
  url: string;
}

export interface PagingObject<T> {
  items: T[];
}

export interface AlbumsResponse {
  albums: PagingObject<Album>;
}
